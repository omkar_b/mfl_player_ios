import Foundation

protocol HomeRouterInput {
    func navigatePranaScreen()
    func navigateDhyanaScreen()
    func navigateMindfulMovementScreen()
    func navigateMindfulEatingScreen()
}
