import UIKit

class HomeRouter:HomeRouterInput {
    
    weak var viewController: HomeViewController!

    func navigatePranaScreen() {
        commonNavigatePranaScreen(vc: viewController)
    }
    
    func navigateDhyanaScreen() {
        commonNavigateDhyanaScreen(vc: viewController)
    }
    
    func navigateMindfulMovementScreen() {
        commonNavigateMindfulMovementscreen(vc: viewController)
    }
    
    func navigateMindfulEatingScreen() {
        commonNavigateMindfulEatingScreen(vc: viewController)
    }
}
