import Foundation

protocol HomeModuleInput: class {
    
    func configureModule()
}
