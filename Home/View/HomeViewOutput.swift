import Foundation

protocol HomeViewOutput {
    func navigatePranaScreen()
    func navigateDhyanaScreen()
    func navigateMindfulMovementScreen()
    func navigateMindfulEatingScreen()
}
