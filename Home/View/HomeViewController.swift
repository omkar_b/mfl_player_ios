import UIKit

class HomeViewController: UIViewController, HomeViewInput {

    var output: HomeViewOutput!
    let network: ConnectivityManager = ConnectivityManager.shared

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkInternetConnetionStatus()
    }
    
    ///This function get the status of rechability.
    func checkInternetConnetionStatus() {
        
        ConnectivityManager.isReachable { networkManagerInstance in
          
        }
        
        ConnectivityManager.isUnreachable { networkManagerInstance in
             UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
               

            }
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                 UIAlertController().internetsetting(vc: self)
            }
        }
    }

    @IBAction func mindfulEatingButtonAction(_ sender: Any) {
       UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Comming Soon..!")
    }
    
    @IBAction func mindfulMovementButtonAction(_ sender: Any) {
      self.output.navigateMindfulMovementScreen()
    }
    
    @IBAction func settingAction(_ sender: Any) {
        UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Comming Soon..!")
    }
    
    @IBAction func dhyanaButtonAction(_ sender: Any) {
        self.output.navigateDhyanaScreen()
    }
    
    @IBAction func pranaButtonAction(_ sender: Any) {
      self.output.navigatePranaScreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//extension UIView{
//    func blink() {
//        self.alpha = 0.2
//        UIView.animate(withDuration: 3, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
//    }
//}
