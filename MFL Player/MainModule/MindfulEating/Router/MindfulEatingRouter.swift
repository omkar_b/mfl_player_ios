import UIKit

class MindfulEatingRouter:MindfulEatingRouterInput {
    
    weak var viewController: MindfulEatingViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
