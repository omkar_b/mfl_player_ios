import Foundation

protocol MindfulEatingRouterInput {
    func back()
}
