import Foundation

protocol MindfulEatingModuleInput: class {
    
    func configureModule()
}
