import UIKit

@objc class MindfulEatingModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = MindfulEatingControllerFromStoryboard()
        
        let router = MindfulEatingRouter()
        router.viewController = viewController
        
        let presenter = MindfulEatingPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = MindfulEatingInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func MindfulEatingControllerFromStoryboard() -> MindfulEatingViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_mindfuleating_vc_id)
        return viewController as! MindfulEatingViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_mainmodule, bundle: Bundle.main)
        return storyboard
    }
    
}
