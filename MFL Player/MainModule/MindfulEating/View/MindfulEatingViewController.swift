import UIKit

class MindfulEatingViewController: UIViewController, MindfulEatingViewInput {

    var output: MindfulEatingViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
