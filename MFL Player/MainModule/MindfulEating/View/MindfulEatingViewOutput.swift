import Foundation

protocol MindfulEatingViewOutput {
    func back()
}
