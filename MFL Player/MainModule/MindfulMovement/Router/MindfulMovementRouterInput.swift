import Foundation

protocol MindfulMovementRouterInput {
    func back()
}
