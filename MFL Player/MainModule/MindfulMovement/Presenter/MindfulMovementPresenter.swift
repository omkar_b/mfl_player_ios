import Foundation

class MindfulMovementPresenter: MindfulMovementModuleInput, MindfulMovementViewOutput, MindfulMovementInteractorOutput {
    
    weak var view: MindfulMovementViewInput!
    var interactor: MindfulMovementInteractorInput!
    var router: MindfulMovementRouterInput!
    
    // MARK: - MindfulMovementViewOutput
    
    func back() {
        router.back()
    }
    func hitMindfulMovementApi(){
        interactor.hitMindfulMovementApi()
    }
    
    func responseMindfulMovement(result:NSDictionary) {
    view.responseMindfulMovement(result:result)
}
    // MARK: - MindfulMovementModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - MindfulMovementInteractorOutput
    
}
