import UIKit

@objc class MindfulMovementModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = MindfulMovementControllerFromStoryboard()
        
        let router = MindfulMovementRouter()
        router.viewController = viewController
        
        let presenter = MindfulMovementPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = MindfulMovementInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func MindfulMovementControllerFromStoryboard() -> MindfulMovementViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_mindfulmovement_vc_id)
        return viewController as! MindfulMovementViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_mainmodule, bundle: Bundle.main)
        return storyboard
    }
    
}
