import Foundation

protocol MindfulMovementViewOutput {
    func back()
    func hitMindfulMovementApi()
}
