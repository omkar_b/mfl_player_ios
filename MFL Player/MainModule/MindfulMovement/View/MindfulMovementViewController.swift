import UIKit
import AVKit
import AVFoundation
import AlamofireImage
import MediaPlayer

class MindfulMovementViewController: UIViewController, MindfulMovementViewInput {
    
    var output: MindfulMovementViewOutput!
    @IBOutlet weak var previousButton : UIButton!
    @IBOutlet weak var nextButton : UIButton!
    @IBOutlet weak var playButton : UIButton!
    @IBOutlet weak var audioDuration : UILabel!
    @IBOutlet weak var artistname : UILabel!
    @IBOutlet weak var songTitle : UILabel!
    @IBOutlet weak var songBackgroundImage : UIImageView!
    var timer:Timer!
    var endTime : String!
    var startTime : String!
    
    let network: ConnectivityManager = ConnectivityManager.shared
    var player: AVPlayer! = nil
    var songsArray : NSArray!
    var index :Int = 0
    let MindfulMovementArray = NSMutableArray()
    let commandCenter = MPRemoteCommandCenter.shared()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        self.output.hitMindfulMovementApi()
        UIApplication.shared.beginReceivingRemoteControlEvents()
        buttonSetup()
        commandCenter.pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the pause command
            self.buttonSetup()
            self.player.pause()
            return .success
        }
        
        commandCenter.playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.buttonSetup()
            self.player.play()
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.nextButtonAction((Any).self)
            self.buttonSetup()
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.previousButtonAction((Any).self)
            self.buttonSetup()
            return .success
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkInternetConnetionStatus()
        
    }
    
    func audioSetup() {
        
        let urldata = MindfulMovementArray.object(at:index) as! MindfulMovementModelData
        if urldata.author == "" {
        } else {
            artistname.text = urldata.author
        }
        
        
        if urldata.songTitle == "" {
            songTitle.text = urldata.title
            
        } else {
            songTitle.text = urldata.songTitle
        }
        if index != 0 {
            let downloadURL = NSURL(string:urldata.backgroundPic)!
            songBackgroundImage.af_setImage(withURL: downloadURL as URL )
        }
        let url = URL(string:urldata.AudioURL)
        player = AVPlayer.init(url: url!)
        let audioAsset = AVURLAsset.init(url: url!, options: nil)
        let duration = audioAsset.duration
        self.endTime =  self.getFormatedTime(FromTime: Int(CMTimeGetSeconds(duration)))
        self.audioDuration.text = "00:00 " + " / " + self.endTime
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .defaultToSpeaker,.allowAirPlay])
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
        player.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions(rawValue: NSKeyValueObservingOptions.new.rawValue | NSKeyValueObservingOptions.old.rawValue), context: nil)
        hideLoading()
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        player.pause()
        self.output.back()
    }
    
    
    @IBAction func downloadAction(_ sender: Any) {
        UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Comming Soon..!")
    }
    
    func responseMindfulMovement(result:NSDictionary) {
        guard let tracklist = result[protocolKeyConstants.RESPONSE_TRACK_LIST] as? NSArray else { return }
        print(tracklist)
        MindfulMovementArray.removeAllObjects()
        for detail in tracklist {
            let details = detail as! NSDictionary
            let modalClass = MindfulMovementModelData().addMindfulMovementDetailswith(
                album: (details.object(forKey: protocolKeyConstants.RESPONSE_ALBUM)as? String ?? "")!,
                author: (details.object(forKey: protocolKeyConstants.RESPONSE_AUTHOR)as? String ?? "")!,
                backgroundPic: (details.object(forKey: protocolKeyConstants.RESPONSE_BACKGROUND_PIC)as? String ?? "")!,
                songTitle: (details.object(forKey: protocolKeyConstants.RESPONSE_DESCRIPTION)as? String ?? "")!,
                enabled: (details.object(forKey: protocolKeyConstants.RESPONSE_ENABLED)as? String ?? "")!,
                length: (details.object(forKey: protocolKeyConstants.RESPONSE_LENGTH)as? String ?? "")!,
                thumbnail: (details.object(forKey: protocolKeyConstants.RESPONSE_THUMBNAIL)as? String ?? "")!,
                title: (details.object(forKey: protocolKeyConstants.RESPONSE_TITLE)as? String ?? "")!,
                trackId: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_ID)as? NSNumber ?? 0)!,
                trackType: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_TYPE)as? String ?? "")!,
                AudioURL: (details.object(forKey: protocolKeyConstants.RESPONSE_URL)as? String ?? "")!,
                udid: (details.object(forKey: protocolKeyConstants.RESPONSE_UDID)as? NSNumber ?? 0)!)
            
            MindfulMovementArray.add(modalClass)
            
        }
        print(MindfulMovementArray)
        audioSetup()
        hideLoading()
    }
    
    ///This function get the status of rechability.
    func checkInternetConnetionStatus() {
        
        ConnectivityManager.isReachable { networkManagerInstance in
            
        }
        
        ConnectivityManager.isUnreachable { networkManagerInstance in
            UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                
                
            }
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                UIAlertController().internetsetting(vc: self)
            }
        }
    }
    
    
    @IBAction func playButtonAction(_ sender: Any) {
        
        if MindfulMovementArray.count == 0 {
            UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "No Songs Available.")
        } else {
            if playButton.currentImage!.isEqual(UIImage(named: ImageConstants.PAUSE_IMAGE) )  {
                player.pause()
                playButtonEnable()
            } else {
                player.play()
                pauseButtomEnable()
            }
        }
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        showLoading()
        index = index - 1
        if index == 0 {
            previousButton.isHidden = true
        }
        if index != MindfulMovementArray.count - 1 {
            nextButton.isHidden = false
        }
        audioSetup()
        playButtonEnable()
        
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        showLoading()
        player.pause()
        index = index + 1
        if index != 0 {
            previousButton.isHidden = false
        }
        if index == MindfulMovementArray.count - 1 {
            nextButton.isHidden = true
        }
        if index > MindfulMovementArray.count - 1 {
            previousButton.isHidden = true
            hideLoading()
            UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "You don't have a next song.")
            
        }
        else{
            audioSetup()
        }
        playButtonEnable()
    }
    
    func buttonSetup() {
        if self.index == 0 {
            commandCenter.previousTrackCommand.isEnabled = false
            commandCenter.nextTrackCommand.isEnabled = true
            
            
        } else if self.index ==  MindfulMovementArray.count - 1 {
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = false
        } else {
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = true
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if player.rate == 1  {
                let interval = CMTime(value: 1, timescale: 1)
                self.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                    let secondsProgress = CMTimeGetSeconds(progressTime)
                    self.startTime = self.getFormatedTime(FromTime: Int(secondsProgress))
                    self.audioDuration.text = self.startTime + " / " + self.endTime
                    self.songCompletioncheck()
                })
                
                print("Playing")
            }else{
                print("Stop")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getFormatedTime(FromTime timeDuration:Int) -> String {
        let minutes = Int(timeDuration) / 60 % 60
        let seconds = Int(timeDuration) % 60
        let strDuration = String(format:"%02d:%02d", minutes, seconds)
        return strDuration
    }
    
    func playButtonEnable() {
        playButton.setImage(UIImage (named: ImageConstants.PLAY_IMAGE), for: .normal)
    }
    
    func pauseButtomEnable() {
        playButton.setImage(UIImage (named: ImageConstants.PAUSE_IMAGE), for: .normal)
    }
    
    func songCompletioncheck() {
        if self.startTime == self.endTime {
            let mutableindex = self.index + 1
            if mutableindex > self.MindfulMovementArray.count - 1 {
                self.index = 0
                self.audioSetup()
                self.playButtonEnable()
            } else {
                self.nextButtonAction((Any).self)
            }
        }
    }
    
}

