import Foundation

protocol MindfulMovementInteractorOutput: class {
    func responseMindfulMovement(result:NSDictionary)
    
}
