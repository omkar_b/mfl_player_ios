import Foundation

class PranaPresenter: PranaModuleInput, PranaViewOutput, PranaInteractorOutput {
    
    weak var view: PranaViewInput!
    var interactor: PranaInteractorInput!
    var router: PranaRouterInput!
    
    // MARK: - PranaViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - PranaModuleInput
    
    func configureModule() {
        
    }
    
    func hitPranaAPI() {
        interactor.hitPranaAPI()
    }
    
    func responsePranaApI(result:NSDictionary) {
        view.responsePranaApI(result:result)

    }

    // MARK: - PranaInteractorOutput
    
}
