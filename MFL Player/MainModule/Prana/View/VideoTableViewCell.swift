//
//  VideoTableViewCell.swift
//  MFL Player
//
//  Created by Omkar on 2/11/19.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoThumbnailImage:UIImageView!
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var timeTitle:UILabel!
    @IBOutlet weak var downloadButton:UIButton!
    @IBOutlet weak var custombackgroundview:UIView!
    @IBOutlet weak var contentview:UIView!



    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected == true
                {
                    self.custombackgroundview.addGradientBackground(firstColor: UIColor (red: 119/255.0, green:91/255.0 , blue: 166/255.0, alpha:1.0), secondColor: UIColor (red: 48/255.0, green: 190/255.0 , blue: 213/255.0, alpha:1.0 ) )
                }
                else
                {
                    self.custombackgroundview.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.4)
                }
        // Configure the view for the selected state
    }
    
}
