import Foundation

protocol PranaViewInput: class {
    
    func responsePranaApI(result:NSDictionary)
}
