import UIKit
import AVKit
import AVFoundation
import AlamofireImage

class PranaViewController: UIViewController, PranaViewInput {

    var output: PranaViewOutput!
    @IBOutlet weak var videoPlayerView : UIView!
    @IBOutlet weak var videoListTableView : UITableView!
     let network: ConnectivityManager = ConnectivityManager.shared
    var player:AVPlayer!
    let controller = AVPlayerViewController()
    let PranaArray = NSMutableArray()
    var index :Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.output.hitPranaAPI()
        self.videoListTableView.register(UINib.init(nibName: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL , bundle: nil), forCellReuseIdentifier: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    ///This function get the status of rechability.
    func checkInternetConnetionStatus() {
        
        ConnectivityManager.isReachable { networkManagerInstance in
            
        }
        
        ConnectivityManager.isUnreachable { networkManagerInstance in
             UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                
                
            }
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                UIAlertController().internetsetting(vc: self)
            }
        }
    }
    
    func responsePranaApI(result:NSDictionary)
    {
        guard let tracklist = result["track_list"] as? NSArray else { return }
        PranaArray.removeAllObjects()
        for detail in tracklist {
            let details = detail as! NSDictionary
            let modalClass = PranaModelData().addPranaDetailswith(
                album: (details.object(forKey: protocolKeyConstants.RESPONSE_ALBUM)as? String ?? "")!,
                author: (details.object(forKey: protocolKeyConstants.RESPONSE_AUTHOR)as? String ?? "")!,
                backgroundPic: (details.object(forKey: protocolKeyConstants.RESPONSE_BACKGROUND_PIC)as? String ?? "")!,
                songTitle: (details.object(forKey: protocolKeyConstants.RESPONSE_DESCRIPTION)as? String ?? "")!,
                enabled: (details.object(forKey: protocolKeyConstants.RESPONSE_ENABLED)as? String ?? "")!,
                length: (details.object(forKey: protocolKeyConstants.RESPONSE_LENGTH)as? String ?? "")!,
                thumbnail: (details.object(forKey: protocolKeyConstants.RESPONSE_THUMBNAIL)as? String ?? "")!,
                title: (details.object(forKey: protocolKeyConstants.RESPONSE_TITLE)as? String ?? "")!,
                trackId: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_ID)as? NSNumber ?? 0)!,
                trackType: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_TYPE)as? String ?? "")!,
                videoURL: (details.object(forKey: protocolKeyConstants.RESPONSE_URL)as? String ?? "")!,
                udid: (details.object(forKey: protocolKeyConstants.RESPONSE_UDID)as? NSNumber ?? 0)!)
            PranaArray.add(modalClass)
        }
        videoListTableView.reloadData()
        self.setUPVideoPlayer()
    }
    
    
    func setUPVideoPlayer() {
        let videodetails = PranaArray.object(at: index) as! PranaModelData
        let videoUrl: URL = URL(string: videodetails.videoURL  )!
        player = AVPlayer(url:videoUrl)
        controller.player = player
        controller.view.frame = self.videoPlayerView.frame
        self.view.addSubview(controller.view)
        self.addChild(controller)
    }
    
    func setUpvideoPlayerPlay() {
        let videodetails = PranaArray.object(at: index) as! PranaModelData
        let videoUrl: URL = URL(string: videodetails.videoURL  )!
        player = AVPlayer(url:videoUrl)
        controller.player = player
        controller.view.frame = self.videoPlayerView.frame
        self.view.addSubview(controller.view)
        self.addChild(controller)
        player.play()
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        self.output.back()
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PranaViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PranaArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL) as! VideoTableViewCell
        let model = self.PranaArray[indexPath.row] as! PranaModelData
        if model.songTitle == "" {
            cell.title.text = model.title
            
        } else {
            cell.title.text = model.songTitle
            
        }
        
        cell.timeTitle.text = model.length
        let downloadURL = NSURL(string:model.thumbnail)!
        cell.videoThumbnailImage.af_setImage(withURL: downloadURL as URL )
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let modelName = UIDevice.modelName
        if modelName == "Simulator iPhone 5s" || modelName == "iPhone 5s" || modelName == "Simulator iPhone SE" || modelName == "iPhone SE" || modelName == "Simulator iPhone 5c" || modelName == "iPhone 5c" {
    
            return 60
        } else if modelName == "Simulator iPhone 6" || modelName == "iPhone 6" || modelName == "Simulator iPhone 6s" || modelName == "iPhone 6s" || modelName == "Simulator iPhone 7" || modelName == "iPhone 7" || modelName == "Simulator iPhone 8" || modelName == "iPhone 8" {
            return 80
        } else {
            return 90 }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL) as! VideoTableViewCell
        player.pause()
        self.index = indexPath.row
        setUpvideoPlayerPlay()
    }
  
}


