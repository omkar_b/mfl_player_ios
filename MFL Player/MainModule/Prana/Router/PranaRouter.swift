import UIKit

class PranaRouter:PranaRouterInput {
    
    weak var viewController: PranaViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
