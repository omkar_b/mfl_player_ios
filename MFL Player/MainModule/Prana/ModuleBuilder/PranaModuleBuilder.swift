import UIKit

@objc class PranaModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = PranaControllerFromStoryboard()
        
        let router = PranaRouter()
        router.viewController = viewController
        
        let presenter = PranaPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = PranaInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func PranaControllerFromStoryboard() -> PranaViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_prana_vc_id)
        return viewController as! PranaViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_mainmodule, bundle: Bundle.main)
        return storyboard
    }
    
}
