//
//  PranaInteractor.swift
//  MFL Player
//
//  Created by Omkar on 2/8/19.
//  Copyright 2019 ___ORGANIZATIONNAME___. All rights reserved.
//
// -----------------------------------------------------------------



import Foundation

class PranaInteractor: PranaInteractorInput {
    
    weak var output: PranaInteractorOutput!
    var net = NetService()

    func hitPranaAPI() {
        net.hitGetAPIWith(url: UrlConstants.PRANA_API) { (value) in
            let result = value as NSDictionary?
            print(result!)
            self.output.responsePranaApI(result : result  as! NSDictionary)

        }
    }

}
