import UIKit

@objc class DhyanaModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = DhyanaControllerFromStoryboard()
        
        let router = DhyanaRouter()
        router.viewController = viewController
        
        let presenter = DhyanaPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = DhyanaInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func DhyanaControllerFromStoryboard() -> DhyanaViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_dhyana_vc_id)
        return viewController as! DhyanaViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_mainmodule, bundle: Bundle.main)
        return storyboard
    }
    
}
