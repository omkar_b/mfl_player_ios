import Foundation

protocol DhyanaModuleInput: class {
    
    func configureModule()
}
