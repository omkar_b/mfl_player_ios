import Foundation

protocol DhyanaViewOutput {
    func back()
    func hitDhyanaApi()
}
