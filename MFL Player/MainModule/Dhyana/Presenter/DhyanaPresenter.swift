import Foundation

class DhyanaPresenter: DhyanaModuleInput, DhyanaViewOutput, DhyanaInteractorOutput {
    
    weak var view: DhyanaViewInput!
    var interactor: DhyanaInteractorInput!
    var router: DhyanaRouterInput!
    
    // MARK: - DhyanaViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - DhyanaModuleInput
    
    func configureModule() {
        
    }
    
    func hitDhyanaApi()
    {
        interactor.hitDhyanaApi()

    }
    
    func responseDhyanaApI(result:NSDictionary){
        view.responseDhyanaApI(result:result)
    }
    // MARK: - DhyanaInteractorOutput
    
}
