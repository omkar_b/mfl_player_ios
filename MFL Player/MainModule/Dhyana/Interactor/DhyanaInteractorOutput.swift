import Foundation

protocol DhyanaInteractorOutput: class {

    func responseDhyanaApI(result:NSDictionary)
}
