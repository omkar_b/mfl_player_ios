//
//  DhyanaInteractor.swift
//  MFL Player
//
//  Created by Omkar on 2/8/19.
//  Copyright 2019 ___ORGANIZATIONNAME___. All rights reserved.
//
// -----------------------------------------------------------------



import Foundation

class DhyanaInteractor: DhyanaInteractorInput {
    
    weak var output: DhyanaInteractorOutput!
    var net = NetService()

    
    func hitDhyanaApi()
    {
        net.hitGetAPIWith(url: UrlConstants.DHYANA_API) { (value) in
            let result = value as NSDictionary?
            print(result)
            self.output.responseDhyanaApI(result : result  as! NSDictionary)
        }
    }
}
