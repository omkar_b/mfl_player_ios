import Foundation

protocol SplashRouterInput {
    func back()
    ///This function is used to navigate to home screen.
    func navigateHomeScreen()

}
