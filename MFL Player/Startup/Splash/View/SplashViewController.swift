import UIKit

class SplashViewController: UIViewController, SplashViewInput {

    var output: SplashViewOutput!
    ///This property network connectivity status.
    let network: ConnectivityManager = ConnectivityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func homeButtonAction(_ sender: Any) {
        self.output.navigateHomeScreen()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
