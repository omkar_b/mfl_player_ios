import Foundation

protocol SplashViewOutput {
    func back()
    func navigateHomeScreen()
}
