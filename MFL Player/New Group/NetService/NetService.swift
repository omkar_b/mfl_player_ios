import Foundation
import Alamofire

class NetService {
    /// This property holds JSON Dictionary.
    var jSON = NSDictionary()
    
    func hitGetAPIWith( url : String, completion: @escaping (NSDictionary?) -> Void) {
        Alamofire.request(
            URL(string: url)!,
            method: .get,
            encoding: JSONEncoding.default,
            headers:["Content-Type":"application/json"])
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while authenticating: \(String(describing: response.result.error))")
                    completion(["result":response.result.error!])
                    return
                }
                if let result = response.result.value {
                    self.jSON = (result as? NSDictionary)!
                    completion(self.jSON)
                } else {
                    completion(["result":response.result.error!])
                }
        }
    }
  
}
