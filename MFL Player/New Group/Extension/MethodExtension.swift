//
//  MethodExtension.swift
//  MFL Player
//
//  Created by Omkar on 2/12/19.
//

import Foundation
import UIKit
import MBProgressHUD

extension UIViewController {
    
    func showLoading() {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.bezelView.style = .solidColor
        loadingNotification.contentColor = UIColor.white
    }
    
    func hideLoading() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
  
}

extension UIAlertController {
    
    func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    func internetsetting(vc: UIViewController) {
        self.hideLoading()
        let alertController = UIAlertController (title: "Mobile Data is Turned off", message: "Turn on mobile data or use Wi-Fi to access data.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
}

extension UIView{
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

