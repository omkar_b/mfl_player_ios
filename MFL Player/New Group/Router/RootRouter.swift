import UIKit

class RootRouter {
    
    var window:UIWindow!
    
    private static let _sharedInstance: RootRouter = RootRouter()
    
    private init() {
        
    }
    
    func showRootViewController(vc viewController:UIViewController, window:UIWindow) {
        let navigationController = navigationControllerFromWindow(window:window)
        navigationController.viewControllers = [viewController];
    }
    
    // MARK: - Private methods
    
    func navigationControllerFromWindow(window:UIWindow) -> UINavigationController {
        let navigationController = window.rootViewController
        return navigationController as! UINavigationController
    }
    
    func showRootViewController(vc viewController:UIViewController ) {
        let navigationController = navigationControllerFromWindow(window:self.window)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    public static  func getRouter() -> (RootRouter){
        return _sharedInstance
    }
}
