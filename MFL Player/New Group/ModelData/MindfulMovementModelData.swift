//
//  MindfulMovementModelData.swift
//  MFL Player
//
//  Created by Omkar on 2/11/19.
//

import UIKit

class MindfulMovementModelData: NSObject {
    var album : String!
    var author : String!
    var backgroundPic : String!
    var songTitle : String!
    var enabled : String!
    var length : String!
    var thumbnail : String!
    var title : String!
    var trackId : NSNumber!
    var trackType : String!
    var AudioURL : String!
    var udid : NSNumber!
    
    
    func addMindfulMovementDetailswith(album:String,author:String,backgroundPic:String,songTitle :String,enabled :String,length:String,thumbnail:String,title:String,trackId:NSNumber,trackType:String,AudioURL:String,udid:NSNumber) -> MindfulMovementModelData {
        
        self.album = album
        self.author = author
        self.backgroundPic = backgroundPic
        self.songTitle = songTitle
        self.enabled = enabled
        self.length = length
        self.thumbnail = thumbnail
        self.title = title
        self.trackId = trackId
        self.trackType = trackType
        self.AudioURL = AudioURL
        self.udid = udid
        return self
        
    }
}
