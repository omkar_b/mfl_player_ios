//
//  PranaModelData.swift
//  MFL Player
//
//  Created by Omkar on 2/11/19.
//

import UIKit

class PranaModelData: NSObject {
    var album : String!
    var author : String!
    var backgroundPic : String!
    var songTitle : String!
    var enabled : String!
    var length : String!
    var thumbnail : String!
    var title : String!
    var trackId : NSNumber!
    var trackType : String!
    var videoURL : String!
    var udid : NSNumber!
    
    
    func addPranaDetailswith(album:String,author:String,backgroundPic:String,songTitle :String,enabled :String,length:String,thumbnail:String,title:String,trackId:NSNumber,trackType:String,videoURL:String,udid:NSNumber) -> PranaModelData {
        
        self.album = album
        self.author = author
        self.backgroundPic = backgroundPic
        self.songTitle = songTitle
        self.enabled = enabled
        self.length = length
        self.thumbnail = thumbnail
        self.title = title
        self.trackId = trackId
        self.trackType = trackType
        self.videoURL = videoURL
        self.udid = udid
        return self
        
    }
}
