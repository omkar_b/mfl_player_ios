import UIKit


protocol CallBackViewWillAppear: class {
    
    func callBackViewWillAppear(controller: UIViewController)
}

class BaseViewController: UIViewController, BaseViewInput, CallBackViewWillAppear {
    
    ///This property knows call back view appear delegate
    weak var callBackViewWillAppearDelegate: CallBackViewWillAppear?
    ///This property knows match model array used for sorting.
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /** This Method will help to Setup the menu width, Color and Presentation Appearance for that particular viewcontroller.*/
   
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hideLeftViewAnimated()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func callBackViewWillAppear(controller: UIViewController) {
        viewWillAppear(true)
    }

     func indicate(_ title:String?, _ message:String?, _ completion: (() -> Void)?) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let defaultButton = UIAlertAction(title: "Ok",
                                          style: .default) {(_) in
                                            if let completion_ = completion {
                                                completion_()
                                            }
        }
        alert.addAction(defaultButton)
        present(alert, animated: true) {
            // completion goes here
        }
    }
}
